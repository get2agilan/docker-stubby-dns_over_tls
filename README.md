Basic Stubby with Cloudflare (1.1.1.1 and 1.0.0.1) DNS over TLS.

Stubby Configuration Options - https://dnsprivacy.org/wiki/display/DP/Configuring+Stubby

Cloudflare DNS - https://1.1.1.1/


```shell
$ docker build dns_over_tls/ -t dns_over_tls
$ sudo docker run --init -p 53:53/udp dns_over_tls:latest

# Verify using dig
$ dig google.com @127.0.0.1
```
